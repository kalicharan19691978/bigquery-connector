package org.mule.extension.internal;

import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.api.connection.ConnectionProvider;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.connection.PoolingConnectionProvider;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Text;

/**
 * This class (as it's name implies) provides connection instances and the
 * funcionality to disconnect and validate those connections.
 * <p>
 * All connection related parameters (values required in order to create a
 * connection) must be declared in the connection providers.
 * <p>
 * This particular example is a {@link PoolingConnectionProvider} which declares
 * that connections resolved by this provider will be pooled and reused. There
 * are other implementations like {@link CachedConnectionProvider} which lazily
 * creates and caches connections or simply {@link ConnectionProvider} if you
 * want a new connection each time something requires one.
 */
public class BasicConnectionProvider implements PoolingConnectionProvider<BasicConnection> {

	@DisplayName("Project Id")
	@Parameter
	private String project_id;
	
	@DisplayName("Private Key Id")
	@Parameter
	private String private_key_id;
	
	@DisplayName("Client Email")
	@Parameter
	private String client_email;
	
	@DisplayName("Client Id")
	@Parameter
	private String client_id;
	
	@DisplayName("Private Key")
	@Text
	@Parameter
	private String private_key;
	
	@Override
	public BasicConnection connect() throws ConnectionException {
		return new BasicConnection(project_id, private_key_id, client_email, client_id, private_key);
	}

	@Override
	public void disconnect(BasicConnection connection) {
		try {
			connection.invalidate();
		} catch (Exception e) {
		}
	}

	@Override
	public ConnectionValidationResult validate(BasicConnection connection) {
		if(connection.getBq() != null)
			return ConnectionValidationResult.success();
		else return ConnectionValidationResult.failure("Connection not initialized.", new Exception("Connection not initialized"));
	}
}
