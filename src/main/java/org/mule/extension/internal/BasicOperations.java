package org.mule.extension.internal;

import static org.mule.runtime.extension.api.annotation.param.MediaType.ANY;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;

import com.google.cloud.bigquery.BigQueryError;
import com.google.cloud.bigquery.FormatOptions;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.QueryParameterValue;
import com.google.cloud.bigquery.TableDataWriteChannel;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.bigquery.WriteChannelConfiguration;

/**
 * This class is a container for operations, every public method in this class
 * will be taken as an extension operation.
 */
public class BasicOperations {

	/**
	 * Example of an operation that uses the configuration and a connection instance
	 * to perform some action.
	 */

	/**
	 * Runs the given SELECT query and returns the results as a TableResult object, which can then be parsed and transformed.
	 */
	
	
	@MediaType(value = ANY, strict = false)
	public TableResult select(String queryString, @Connection BasicConnection connection,ParameterResolver<Map<String,String>> inParam) {
	    
		QueryJobConfiguration.Builder queryConfigBuilder =
			    QueryJobConfiguration.newBuilder(queryString);
		for(Map.Entry<String, String> param: inParam.resolve().entrySet()) {
			queryConfigBuilder.addNamedParameter(param.getKey(), QueryParameterValue.string(param.getValue()));
		}
		QueryJobConfiguration queryConfig = queryConfigBuilder.build();
		TableResult res = null;
		try {
		    res = connection.getBq().query(queryConfig);
		} catch (JobException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	
	/**
	 * 
	 */
	public void update(String queryString, @Connection BasicConnection connection) {
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(queryString).build();
		try {
			connection.getBq().query(queryConfig);
		} catch (JobException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Bulk inserts data from a csv source. Requires job.create permissions
	 */
	public void insertBulk(String projectName, String datasetName, String tableName, @Connection BasicConnection connection, InputStream csvData) throws IOException, InterruptedException {
		TableId tableId = TableId.of(projectName, datasetName, tableName);
		WriteChannelConfiguration writeChannelConfiguration = WriteChannelConfiguration.newBuilder(tableId)
				.setFormatOptions(FormatOptions.csv())
				.build();
		TableDataWriteChannel writer = connection.getBq().writer(writeChannelConfiguration);
		
		try {
			writer.write(ByteBuffer.wrap(IOUtils.toByteArray(csvData)));
		} finally {
			writer.close();
		}
		Job job = writer.getJob();
		job = job.waitFor();
		if (job.getStatus().getError() != null) {
			System.out.println(job.getStatus().getError().toString());
		} else {
			return;
		}
	}
	
	/**
	 * Inserts data by streaming using the Table.insertAll method. Limits and quotas apply. Batches of 500 recommended.
	 */
	public void insertStream(String projectName, String datasetName, String tableName, @Connection BasicConnection connection, List<Map<String, Object>> data) {
		TableId tableId = TableId.of(projectName, datasetName, tableName);
		InsertAllRequest.Builder b = InsertAllRequest.newBuilder(tableId);
		
		for(Map<String, Object> row : data) {
			b.addRow(row);
		}
		InsertAllResponse response = connection.getBq().insertAll(b.build());
		
		if (response.hasErrors()) {
			  // If any of the insertions failed, this lets you inspect the errors
			  for (Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
			    System.out.println(entry.toString());
			  }
		}
	}
	
	public void updateProcessState(String projectName, String datasetName, String tableName, ArrayList<String> uuids, @Connection BasicConnection connection) {
		StringBuffer query = new StringBuffer("UPDATE `" + projectName + "." + datasetName + "." + tableName + "` SET process_flag = 'P' WHERE UUID IN (");
		for(int i = 0; i < uuids.size(); i++) {
			query.append("\'" + uuids.get(i) + "\'");
			if(i < uuids.size() - 1)
				query.append(", ");
		}
		query.append(");");
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query.toString()).build();
		try {
			connection.getBq().query(queryConfig);
		} catch (JobException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
