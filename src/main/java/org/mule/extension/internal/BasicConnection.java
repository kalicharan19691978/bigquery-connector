package org.mule.extension.internal;

import java.io.IOException;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;

/**
 * This class represents an extension connection just as example (there is no
 * real connection with anything here c:).
 */
public final class BasicConnection {
	private BigQuery bq = null;

	public BasicConnection(String project_id, String private_key_id, String client_email, String client_id, String private_key) {
		 
			ServiceAccountCredentials credentials = null;
			try {
				credentials = ServiceAccountCredentials.fromPkcs8(client_id, client_email, private_key, private_key_id, null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bq = BigQueryOptions.newBuilder().setCredentials(credentials).setProjectId(project_id).build()
					.getService();
	
	}

	public BigQuery getBq() {
		return bq;
	}

	public void invalidate() {
		
	}
}
