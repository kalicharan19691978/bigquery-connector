package bqtest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.mule.extension.internal.BasicConnection;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.runtime.parameter.ParameterResolver;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryError;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableResult;

public class bqtest {

	public static void main(String[] args) {
		String project_id = "sq-fbs-datastore-stage", 
				private_key_id = "1d510741ba8de78d7770783972a4d57cd6ac47b6",
				private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCvqN4jlKRrun0i\n6/TuhJx1fXuiqawA2WIcpSjI1bVtlyQSn0Lcyim8mQfos6a9/SKntb7AtAKYEozj\nG4qHBMQNiz2rOfL/Hi1D71OxkueGYWRvsnz5Knsd+S8mCM/UjL+A62CQ5t0ppYrI\n8jx0pMMo3Juv4JNDfzyOUgS4K0FpWnrvOPF8aESA/DvL2D7FQwkFuhAF+ES0MFxh\nCXbGk71y2VyOG18gC0/G187NTnZwkzotiriFLBAkK+C1Zb1QP5nlK8HEmrld7R6I\noDFN0m+rEVUlD9lYdRwNnzIIF0RRkWot8ZpNx5EfEfpcN7bR2SNCZdkivcicUidl\njygVFoYHAgMBAAECggEABmFOTDPTjm5q8v3VbUAXqUbLSJZFNf3T3jqgCdCFToSf\neUqJa2H4q2S/0JONTIEgJS2yGAva4R9GOJbqR++WToIzHpI0L1CvNKJBw5eUb6FI\nqhmkB/daP0fPfKk9IYJJNF28zl+sQuZ6/3q9eBtmeBqLve5l33iErjCnJ19MiyES\ner+jCEf7alcpyMrRIn3EpIGtelaqVGa+zmq5z7z5PtrroeJRJBAl/KPu3gSwhKzA\nKzlEFUMgn3Ynp3aWJL4SJVbE0pJ49yICBjwbikkgpvrS651dFGiF/UH5zrYa58+w\n8Gn7Y1TOAMlfJKJbwO60JiP9nGQ2TGB1l4kLi/S3HQKBgQDrF9/SyRZb+sh0oWyn\nm++gpQSzTfXIU8hr501CppSG3J7g3obvhJuIVgqa40dUscBSmM//P6KUcwomcRC+\nRbAdM9SlP+DXQaBXRf+Kke3ELA7ApfBZdNEJxgT25SwAcnGsUgWv+ywphKD5s882\nsgq46yGvQFRTa7f1c+atCe5f5QKBgQC/R+31KGQQBUyCDmaukNmih/WXpxoNHq3a\nbSQblcqj0bET9mmfN54MsVyCGSprKx4gnzrbG4J5aJOtLAIawroSge/Hpny1RHym\nijdu5dtduHNK0XVDPfA/qESQGfLABzCvHxvBrq9TjSRiCnm2XaCXd0MFzrQsvWJb\ny6/gj193ewKBgAMmSLtcMk59J55YY0pDjoxKaau1SaurY63+ZUyVSrwbzI0arhgb\nYLIM5Qt6LAR4dSkmHvM+eQEgIWyuaxkBegCKJzknh+2aVXmV2HyUw+iy5meyUWwB\nYqOM4ZglLde7sMYsOyNGbCsKOdXekNIcxIQBsmvUosyTTt2c9eQvk9/5AoGBAIyu\n0ZQS/e1OiGPLRbg0buz8RCQi0aU+oC1bQkhLljuAeVWuUSa6GDNLqzdAXRNvJfxm\nQDpwdDjpMlwWVwHpoEMGcB2OFICZe3hdhcG5quq2Q42Omkk3X+Dh2HOHsiOL5kGW\nhqqf2PGBiHXXMIdxOzDLlW7LBXc2XwYWvCdUK6nLAoGBANNI2raadJ8gFnY9Vktr\nc1cvglp7cRLhMv6VCaI5v4Xqf4qCpfpNcu62MPWL//tMmIS6EYlqxZzpK8iv5Uv4\nGv6/ihOVCkD+nTC2Ba9ZEtewXR7Ca/ZZ88OpOROMR9bvIpBvXWoZMEIPISp8rG+h\nDutoSGxOr2uMpwXzgc/Raqyp\n-----END PRIVATE KEY-----\n", 
				client_email = "fbs-staging-fred3-connection@sq-fbs-datastore-stage.iam.gserviceaccount.com", 
				client_id = "106797815842245612397";
		ServiceAccountCredentials credentials = null;
		try {
			credentials = ServiceAccountCredentials.fromPkcs8(client_id, client_email, private_key, private_key_id, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BigQuery bq = BigQueryOptions.newBuilder().setCredentials(credentials).setProjectId(project_id).build()
				.getService();
		bq.toString();
		BasicConnection bs = new BasicConnection(project_id, private_key_id, client_email, client_id, private_key);
		
		ArrayList<String> uuids = new ArrayList<String>();
		uuids.add("f6770107-8b8b-4813-a2e7-d44c369680ba");
		uuids.add("2fcbbe0a-853a-4ebe-abdc-246e1022bb83");
		uuids.add("87ca08c4-db88-42d8-8c5f-b77747883d41");
		select("SELECT * FROM `reconciliation-development.development_mulesoft_errors.weebly_mulesoft_errors` limit 100;", bs, null);
		updateProcessState("reconciliation-development", "development_mulesoft_errors", "weebly_mulesoft_errors", uuids, bs);
		
		StringBuffer query = new StringBuffer("UPDATE `reconciliation-development.development_mulesoft_errors.weebly_mulesoft_errors` SET process_flag = 'P' WHERE UUID IN (");
		for(int i = 0; i < uuids.size(); i++) {
			query.append("\'" + uuids.get(i) + "\'");
			if(i < uuids.size() - 1)
				query.append(", ");
		}
		query.append(");");
		System.out.println(query.toString());
		
	}
	
	public static void insertStream(String projectName, String datasetName, String tableName, @Connection BasicConnection connection, List<Map<String, Object>> data) {
		TableId tableId = TableId.of(projectName, datasetName, tableName);
		InsertAllRequest.Builder b = InsertAllRequest.newBuilder(tableId);
		
		for(Map<String, Object> row : data) {
			b.addRow(row);
		}
		InsertAllResponse response = connection.getBq().insertAll(b.build());
		if (response.hasErrors()) {
			  // If any of the insertions failed, this lets you inspect the errors
			  for (Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
			    System.out.println(entry.toString());
			  }
		}
	}
	
	public static void updateProcessState(String projectName, String datasetName, String tableName, ArrayList<String> uuids, @Connection BasicConnection connection) {
		StringBuffer query = new StringBuffer("UPDATE `" + projectName + "." + datasetName + "." + tableName + "` SET process_flag = 'P' WHERE UUID IN (");
		for(int i = 0; i < uuids.size(); i++) {
			query.append("\'" + uuids.get(i) + "\'");
			if(i < uuids.size() - 1)
				query.append(", ");
		}
		query.append(");");
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query.toString()).build();
		try {
			connection.getBq().query(queryConfig);
		} catch (JobException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static TableResult select(String queryString, @Connection BasicConnection connection,ParameterResolver<Map<String,String>> inParam) {
		QueryJobConfiguration.Builder queryConfigBuilder =
			    QueryJobConfiguration.newBuilder(queryString);
//		for(Map.Entry<String, String> param: inParam.resolve().entrySet()) {
//			queryConfigBuilder.addNamedParameter(param.getKey(), QueryParameterValue.string(param.getValue()));
//		}
		QueryJobConfiguration queryConfig = queryConfigBuilder.build();
		TableResult res = null;
		try {
		    res = connection.getBq().query(queryConfig);
		} catch (JobException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return res;
	}

}
